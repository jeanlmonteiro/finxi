#!/bin/bash
include docker/common.env

.SILENT:
.PHONY: clean


default: help

help:
	@echo ""
	@echo "-- Help Menu"
	@echo ""

runserver:
	docker-compose run --rm --service-ports -e "COMMAND=runserver" django

migrate:
	docker-compose run --rm django python manage.py migrate --noinput

collectstatic:
	docker-compose run --rm django python manage.py collectstatic --noinput

test:
	docker-compose run --rm django python manage.py test

shell: stop
	docker-compose run --rm --service-ports django bash

shell-root: stop
	docker-compose run --rm --service-ports -u root django bash

_build:
	docker-compose build

build: _build stop clean-layers

_start:
	docker-compose up -d

start: _start collectstatic migrate

stop:
	docker-compose down

restart: stop start

log:
	docker-compose logs --follow

clean-containers:
	-docker ps -aqf ancestor=${PROJECT_NAME}_* | xargs docker rm -f

clean-images:
	-docker images -q -f reference=${PROJECT_NAME}_* | xargs docker rmi -f

clean-layers:
	-docker images -q -f dangling=true | xargs docker rmi -f

clean-volumes:
	-docker volume ls -q -f name=${PROJECT_NAME}_* | xargs docker volume rm -f

clean-all: stop clean-containers clean-images clean-layers clean-volumes
