from django.urls import path

from . import views


app_name = 'imoveis'
urlpatterns = [
    path('', views.ImovelIndexView.as_view(), name='index'),
    path('add/', views.ImovelCreateView.as_view(), name='create'),
    path('<int:pk>/', views.ImovelDetailView.as_view(), name='detail'),
    path('<int:pk>/edit/', views.ImovelUpdateView.as_view(), name='update'),
    path('<int:pk>/delete/', views.ImovelDeleteView.as_view(), name='delete'),
]
