from django.db import models
from django.db.models.signals import post_save
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.urls import reverse
from django.utils.translation import gettext as _

from apps.addresses.models import AbstractAddress


def photo_directory_path(instance, filename):
    import os
    import uuid

    filename = '{}.{}'.format(uuid.uuid4().hex, filename.split('.')[-1])
    return os.path.join('imoveis', filename)


class Imovel(AbstractAddress):
    quartos = models.PositiveSmallIntegerField(_('quartos'))
    suites = models.PositiveSmallIntegerField(
        _('suítes'), null=True, blank=True)
    vagas_garagem = models.PositiveSmallIntegerField(
        _('vagas de garagem'), null=True, blank=True)
    area_util = models.PositiveSmallIntegerField(_('área útil (M²)'))
    area_total = models.PositiveSmallIntegerField(
        _('área total (M²)'), null=True, blank=True)
    aluguel = models.DecimalField(
        _('aluguel'), max_digits=10, decimal_places=2)
    condominio = models.DecimalField(
        _('condomínio'), max_digits=10, decimal_places=2, null=True,
        blank=True)
    iptu = models.DecimalField(
        _('IPTU'), max_digits=10, decimal_places=2, null=True, blank=True)
    descricao = models.TextField(_('descrição'), null=True, blank=True)
    image = models.ImageField(_('image'), upload_to=photo_directory_path)
    thumbnail = models.ImageField(
        _('thumbnail'), upload_to=photo_directory_path, null=True,
        editable=False)

    class Meta:
        verbose_name = _('imóvel')
        verbose_name_plural = _('imóveis')

    def get_absolute_url(self):
        return reverse('imoveis:detail', kwargs={'pk': self.pk})


@receiver(pre_save, sender=Imovel)
def imovel_pre_save(sender, instance, *args, **kwargs):
    if instance.pk:
        old = Imovel.objects.get(pk=instance.pk)
        if old.image.url != instance.image.url:
            instance.thumbnail = None


@receiver(post_save, sender=Imovel)
def imovel_post_save(sender, instance, *args, **kwargs):
    from .tasks import create_thumbnail

    if not instance.thumbnail:
        create_thumbnail.delay(instance.pk)
