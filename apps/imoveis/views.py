from django.db.models import Q
from django.urls import reverse_lazy
from django.views.generic import DetailView
from django.views.generic import ListView
from django.views.generic.edit import CreateView
from django.views.generic.edit import DeleteView
from django.views.generic.edit import UpdateView


from .models import Imovel


class ImovelIndexView(ListView):
    model = Imovel

    def get_queryset(self):
        query = self.request.GET.get('q')
        if not query:
            return Imovel.objects.all()

        return Imovel.objects.filter(
            Q(city__search=query) |
            Q(neighborhood__search=query) |
            Q(street__search=query) |
            Q(postal_code__search=query)
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['q'] = self.request.GET.get('q')
        return context


class ImovelCreateView(CreateView):
    model = Imovel
    fields = '__all__'


class ImovelDetailView(DetailView):
    model = Imovel


class ImovelUpdateView(UpdateView):
    model = Imovel
    fields = '__all__'


class ImovelDeleteView(DeleteView):
    model = Imovel
    success_url = reverse_lazy('imoveis:index')
