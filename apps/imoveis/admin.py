from django.contrib import admin
from django.utils.html import format_html

from .models import Imovel


@admin.register(Imovel)
class ImovelAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'thumbnail_img')
    list_display_links = None
    fieldsets = (
        (None, {
            'fields': (
                ('quartos', 'suites'),
                ('area_util', 'area_total'),
                ('aluguel',),
                ('iptu', 'condominio'),
                ('descricao',),
            )
        }),
    )

    def get_actions(self, request):
        actions = super().get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def thumbnail_img(self, obj):
        if obj.thumbnail:
            return format_html(
                '<img src="{}" height="150" width="150" />', obj.thumbnail.url)

    thumbnail_img.short_description = 'thumbnail'
    thumbnail_img.empty_value_display = 'unknown'
