from __future__ import absolute_import, unicode_literals
from celery import shared_task

from .models import Imovel


@shared_task
def create_thumbnail(imovel_pk):
    import sys
    import uuid
    from io import BytesIO

    from django.core.files.uploadedfile import InMemoryUploadedFile
    from PIL import Image

    imovel = Imovel.objects.get(pk=imovel_pk)

    image = Image.open(imovel.image)

    output = BytesIO()
    im = image.resize((150, 150))
    im.save(output, format='JPEG', quality=100)
    output.seek(0)

    imovel.thumbnail = InMemoryUploadedFile(
        output,
        'ImageField',
        "{}.jpg".format(uuid.uuid4().hex),
        'image/jpeg',
        sys.getsizeof(output),
        None
    )

    imovel.save()
