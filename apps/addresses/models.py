from django.db import models
from django.utils.translation import gettext as _


class AbstractStreet(models.Model):
    country = models.CharField(_('country'), max_length=50)
    state = models.CharField(_('state'), max_length=50)
    city = models.CharField(_('city'), max_length=50)
    neighborhood = models.CharField(_('neighborhood'), max_length=50)
    street = models.CharField(_('street'), max_length=50)

    class Meta:
        abstract = True
        verbose_name = _('street')
        verbose_name_plural = _('streets')

    @property
    def neighborhood_full_name(self):
        return _('{neighborhood}, {city} - {state}').format(
            neighborhood=self.neighborhood,
            city=self.city,
            state=self.state,
            country=self.country,
        )

    @property
    def street_full_name(self):
        return _('{street} - {neighborhood}').format(
            street=self.street, neighborhood=self.neighborhood_full_name)


class AbstractAddressDetail(models.Model):
    postal_code = models.CharField(_('postal code'), max_length=20)
    number = models.CharField(_('number'), max_length=50)
    complement = models.CharField(
        _('complement'), max_length=50, null=True, blank=True)
    reference = models.CharField(
        _('reference'), max_length=100, null=True, blank=True)

    class Meta:
        abstract = True
        verbose_name = _('address')
        verbose_name_plural = _('addresses')

    @property
    def address_full_name(self):
        return _('{street}, {number} - {neighborhood}, {postal_code}').format(
            street=self.street,
            number=self.number,
            neighborhood=self.neighborhood_full_name,
            postal_code=self.postal_code,
        )


class AbstractAddress(AbstractStreet, AbstractAddressDetail):

    class Meta:
        abstract = True

    def __str__(self):
        return self.address_full_name


class AbstractAddressHistory(AbstractAddress):

    class Meta:
        abstract = True
        verbose_name = _('address history')
        verbose_name_plural = _('address history')
