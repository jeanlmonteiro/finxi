from .default import *


DEBUG = False

ADMINS = (
    ('Jean Monteiro', 'jeanlmonteiro@gmail.com'),
)

INSTALLED_APPS = INSTALLED_APPS + [
    'django.contrib.postgres',
    'django_celery_beat',
    'django_celery_results',

    'apps.addresses',
    'apps.imoveis',
]


DATABASES = {
    'default': {
        'ENGINE': os.getenv('DB_ENGINE'),
        'NAME': os.getenv('DB_DB_NAME'),
        'USER': os.getenv('DB_USERNAME'),
        'PASSWORD': os.getenv('DB_PASSWORD'),
        'HOST': os.getenv('DB_HOSTNAME'),
        'PORT': os.getenv('DB_PORT', ''),
    }
}


SECRET_KEY = os.getenv('SECRET_KEY')


MEDIA_URL = '/media/'
MEDIA_ROOT = 'media/'

STATIC_URL = '/static/'
STATIC_ROOT = 'static/'


# CACHES
CACHES = {
    'default': {
        'BACKEND': 'django_redis.cache.RedisCache',
        'LOCATION': os.getenv('CACHES_DEFAULT', 'unique-snowflake'),
        'OPTIONS': {
            'PARSER_CLASS': 'redis.connection.HiredisParser',
        },
    },
    'sessions': {
        'BACKEND': 'django_redis.cache.RedisCache',
        'LOCATION': os.getenv('CACHES_SESSIONS', 'unique-snowflake'),
        'OPTIONS': {
            'CLIENT_CLASS': 'django_redis.client.DefaultClient',
            'PARSER_CLASS': 'redis.connection.HiredisParser',
        },
    },
    'local': {
        'BACKEND': 'django_redis.cache.RedisCache',
        'LOCATION': 'unique-snowflake',
    },
}

SESSION_ENGINE = 'django.contrib.sessions.backends.cache'
SESSION_CACHE_ALIAS = 'sessions'


# CELERY
CELERY_BEAT_SCHEDULER = 'django_celery_beat.schedulers:DatabaseScheduler'
CELERY_BROKER_URL = os.getenv('CELERY_BROKER_URL')
CELERY_ACCEPT_CONTENT = ['json']
CELERY_RESULT_SERIALIZER = 'json'
CELERY_TASK_SERIALIZER = 'json'
CELERY_TIMEZONE = TIME_ZONE
CELERY_RESULT_PERSISTENT = True
CELERY_RESULT_BACKEND = os.getenv('CELERY_RESULT_BACKEND', 'django-db')
