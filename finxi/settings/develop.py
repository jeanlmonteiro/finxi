from .staging import *


DEBUG = True

ALLOWED_HOSTS = ['*']

# EMAIL
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'


###############################
# debug_toolbar
INSTALLED_APPS = INSTALLED_APPS + [
    'debug_toolbar',
]

MIDDLEWARE = [
    'debug_toolbar.middleware.DebugToolbarMiddleware',
] + MIDDLEWARE

DEBUG_TOOLBAR_CONFIG = {
    'SHOW_TOOLBAR_CALLBACK': lambda request: True,
}


###############################
# django_nose
INSTALLED_APPS = INSTALLED_APPS + [
    'django_nose',
]

TEST_RUNNER = 'django_nose.NoseTestSuiteRunner'
NOSE_ARGS = [
    '--with-coverage',
    '--cover-erase',
    '--cover-html',
    '--cover-package=apps/',
]
