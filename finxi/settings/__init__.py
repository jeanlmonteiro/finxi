from .develop import *

try:
    from .local_settings import *
    print('Warning: local_settings (unversioned) imported to settings.')
except ImportError:
    pass
