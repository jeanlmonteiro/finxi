#!/bin/bash

set -e

if [[ $COMMAND = "celeryworker" ]]; then
    echo "Running Celery Worker"
    exec celery worker -E -A ${PROJECT_NAME} --loglevel=${LOGLEVEL}

elif [[ $COMMAND = "celerybeat" ]]; then
    echo "Running Celery Beat"
    exec celery beat -A ${PROJECT_NAME} --loglevel=${LOGLEVEL} --pidfile=/tmp/celerybeat.pid

elif [[ $COMMAND = "celeryflower" ]]; then
    echo "Running Celery Flower"
    exec celery flower -A ${PROJECT_NAME} --logging=${LOGLEVEL}

elif [[ $COMMAND = "runserver" ]]; then
    echo "Running manage.py runserver"
    exec python manage.py runserver 0.0.0.0:8000

elif [[ $COMMAND = "UNIT" ]]; then
    echo "Running Unit Tests"
    exec python "tests.py"

else
    echo "Running uWSGI Server"
    exec uwsgi ${WORKSPACE}/docker/django/uwsgi.ini
fi
